import socket
import threading
import dill
import zlib
import hashlib
import time
import sys
import importlib
import os
import inspect

from enum        import Enum

# Codici di messaggi del protocollo di comunicazione
class MsgPrt( Enum ):
  UNSET                 = 0
  # Risposte generiche
  ACK			= 1
  ERROR                 = 2
  RESEND                = 3
  UNEXPECTED            = 4
  # Messaggi generati dal server relativi alle credenziali
  NOT_LOGGED		=10	# Il client non ha fatto login.	-> E_NoLogin
  CREDNOTVALD		=11	# Credenziali non valide.	-> E_CredNonVald
  NOT_ADMIN             =12	# Il client non e' un admin	-> E_NotAdmin
  # Comandi generati dal client
  SIGNUP		=100
  LOGIN			=110
  ADM_DEBUG		=120
  ADM_CLIENT_DB		=130
  ADM_CLIENT_ONLINE	=140
  JOBREQUEST		=150
  JOBDONE		=160
  # Messaggi generati dal server relativi ai comandi ricevuti
  SIGNUP_OK		=200
  LOGIN_OK		=210
  ADM_DEBUG_OK          =220
  ADM_CLIENT_DB_OK	=230
  ADM_CLIENT_ONLINE_OK	=240
  JOBREQUEST_CALC	=250
  JOBREQUEST_NOTHING	=251	# Non ci sono task da elaborare	-> E_NothingToDo 

# Eccezioni personalizzate

class E_Login( BaseException ):
  pass
class E_NoLogin( E_Login ):
  pass
class E_CredNonVald( E_Login ):
  pass
class E_NotAdmin( E_Login ):
  pass

class E_Exec( BaseException ):
  pass
class E_DataCorrupt( E_Exec ):
  pass
class E_MissVer( E_Exec ):
  pass
class E_WrongVer( E_Exec ):
  pass
class E_ExecError( E_Exec ):
  pass
class E_MsgUnexpected( E_Exec ):
  pass
class E_NothingToDo( E_Exec ):
  pass
class E_FileName( E_Exec ):
  pass
class E_MaxTaskPerJob( E_Exec ):
  pass

class E_RecallFunct( BaseException ):
  pass
class E_RecallStop( E_RecallFunct ):
  pass
class E_RecallAgain( E_RecallFunct ):
  pass
class E_RecallMax( E_RecallFunct ):
  pass
  
# Enum con valori int da rappresentare in str
class IntStrEnum( Enum ):
  def __int__( self ):
    return self.value
  def __index__( self ):
    return self.value
  def __add__( self, other ):
    return "%s%s"%(self, other)
  def __radd__( self, other ):
    return "%s%s"%(other, self)
  def __format__(self, fs):
    return str( self )

# Stato di job e tasks
class JobStatus( IntStrEnum ):
  UNSET                 = 0
  NEW			= 1
  WORKING		= 2
  DONE			= 3
  ERROR			= 4
  RESEND                = 5
  def __str__(self):
    return str( ['New', 'Working', 'Done', 'Error', 'Resend'][ self.value ] )
class TaskStatus( IntStrEnum ):
  UNSET                 = 0
  NEW			= 1
  WORKING		= 2
  DONE			= 3
  ERROR			= 4
  def __str__(self):
    return str( ['New', 'Working', 'Done', 'Error'][ self.value ] )

# Colori
class Color( Enum ):
  RESET                 = "\033[0;0m"
  RED                   = "\033[1;31m"  
  GREEN                 = "\033[1;32m"
  YELLOW                = "\033[1;33m"
  PURPLE                = "\033[1;35m"
  CYAN                  = "\033[1;36m"
  def __str__(self):
    return str( self.value )
  def __format__(self, fs):
    return str( self.value )
  def __add__( self, other ):
    return "%s%s"%(self, other)
  def __radd__( self, other ):
    return "%s%s"%(other, self)
    

class PyCalDis( object ):

  def __init__( self, host='', port=4096, max_data_file=16 ):
    
    # Set UTF-8
    reload( sys )
    sys.setdefaultencoding('utf8')

    # Inizializzazione
    self.version    = 1.4

    self.host       = host
    self.port       = port
    self.bufsize    = 1024*1 # 1k buffer, poi imposta quello del sock

    # Visualizzazione messaggi    
    self.printdebug = False		# Messaggi debug
    self.printalert = True		# Messaggi alert
    self.printerr   = True		# Messaggi di errore
 
    '''
      Dati che vengono salvati su file
        jobs
        clientdb
    '''
    self.data                      = {} 		# Struttura dati che viene salvata su file
    self.jobs                      = {}                 # Dictionary dei jobs, viene salvato un job per file
    self.data['io']                = {}                 # Gestione IO
    self.data['io']['fname']       = '' 		# Nome datafile. Se non viene settato non viene salvato niente
    self.data['io']['changed']     = False		# Richiede salvataggio dati
    self.data['io']['reading']     = False		# Lettura in corso
    self.data['io']['writing']     = False		# Salvataggio in corso
    self.data['io']['maxdatafile'] = max_data_file      # Numero massimo di datafile da tenere in memoria per essere distribuiti ( status != DONE )
    # thread __start_autosave
    self.th_autosave               = None	        # Thread autosave
    self.autosaving                = False	        # Inizializzazione flag, viene settato quando il loop __start_autosave e' in esecuzione 
    self.loop_autosave             = False 	        # Se True il loop e' attivo, False il loop __start_autosave  esce
    self.autosave_last             = time.time()        # Ultima esecuzione autosalvataggio
    self.autosave_timeout          = 60*10	        # Timeout per far scattare l'autosalvataggio

    '''
      Dati che vengono persi alla chiusura
        token
    '''
    self.onlinedata = {}

    ''' -------------------------------------------------------------------------
    Init jobs
      Stessa struttura dati presente sia nel server che nel client.
    ''' 
    if 'jobs_io' not in self.data:
      self.data['jobs_io'] = {}                         # Contiene gli id dei job con il relativo nome file
    self.assigning_job          = False                 # Flag che segnala l'assegnamento di un job per evitare concorrenza di assegnazioni fra thread diversi
    self.data['next_job_id']	= 0                     # contatore che assicura sempre un id univoco
    self.data['next_task_id']	= 0                     # contatore che assicura sempre un id univoco
    self.job_assign_timeout     = 10*60	                # Se il task non viene completato in tot secondi dall' invio viene resettato per la riassegnazione
 
    # Numero massimo di task da inviare in un job
    self.max_tasks_per_job = 1000    

 
  def __del__( self ):
    self.__stop_autosave()
    self.write()

  ''' -------------------------------------------------------------------------
    Metodi debug
  '''
    
  def dumpdata( self ):
    print "host     : {0}".format( self.host     )
    print "port     : {0}".format( self.port     )
    print "bufsize  : {0}".format( self.bufsize  )
    print "datafile : {0}".format( self.data['io']['fname'] )
    print "savedata : {0}".format( self.data['io']['changed'] )
    print "data     : {0}".format( self.data     )

  def debug( self, Mess="" ):
    if self.printdebug:
      call = inspect.stack()
      lcal = []
      for i in reversed( range( 1, len( call )-1 ) ):
        lcal.append( call[i][3] )
      Spazi = " "*( 2 * ( len( call )-2 ) )
      print ( Color.CYAN + "D:" + Color.RESET + Spazi + Color.GREEN + "{0}" + Color.RESET + ": '{1}'" ).format( ".".join(lcal), Mess )

  def printErr( self, Err, Mess="" ):
    if self.printerr:
      call = inspect.stack()
      lcal = []
      for i in reversed( range( 1, len( call )-1 ) ):
        lcal.append( call[i][3] )
 
      if 'errno' in Err:
        print ( Color.RED + "E:" + Color.GREEN + "{0}" + Color.RESET + ": '{1}' - {2} - {3}" ).format( ".".join(lcal), Mess, Err.errno, Err.strerror )
      else:
        print ( Color.RED + "E:" + Color.GREEN + "{0}" + Color.RESET + ": '{1}' - {2}"       ).format( ".".join(lcal), Mess, Err )

  def alert( self, Mess="" ):
    if self.printalert:
      call = inspect.stack()
      Spazi = " "*( 2 * ( len( call )-2 ) )
      print ( Color.YELLOW + "A:" + Color.RESET + Spazi + "'{0}'" ).format( Mess )

  def show_io( self ):
    print Color.GREEN + "IO Datafile:" + Color.RESET
    print self.data['io']
    print Color.GREEN + "IO Jobs" + Color.RESET
    for k, v in self.data['jobs_io'].items():
      print "{0:8d}{1}".format( k, v ) 
    print Color.GREEN + "Jobs IDs" + Color.RESET
    print self.jobs.keys()

  def show_th( self ):
    te = threading.enumerate ()
    print"{0:>16s}{1:>6s}".format( "Thread", "Vivo" )
    for th in te:
      print "{0:>16}{1:>6}".format( th.getName(), th.isAlive() )

  ''' -------------------------------------------------------------------------
    serializzazione+compressione / decompressione+deserializzazione stringhe
  '''

  def zipobj( self, objdata ):
    try:
      s_objdata = dill.dumps( objdata )
      z_objdata = zlib.compress( s_objdata )
      self.debug( "Compressione: {0:<.2f}% Byte: {1} -> {2}".format( 100.0 * len( z_objdata ) / len ( s_objdata ), len( s_objdata ), len( z_objdata ) ) )
      return z_objdata
    except Exception as e:
      self.printErr( Err=e, Mess="" )
      raise

  def unzipobj( self, zipdata ):
    # zipdata puo' essere vuoto
    try:
      if zipdata != "":
        s_zipdata = zlib.decompress( zipdata )
        o_zipdata = dill.loads( s_zipdata )
        self.debug( "Compressione: {0:<.2f}% Byte: {1} -> {2}".format( 100.0 * len( zipdata ) / len ( s_zipdata ), len( zipdata ), len( s_zipdata ) ) )
        return o_zipdata
      else:
        return ""
    except Exception as e:
      self.printErr( Err=e, Mess="" )
      raise

  ''' -------------------------------------------------------------------------
    Metodi invio / ricezione dati
    
    Tutti questi metodi sollevano eccezioni che vanno gestite da chi li chiama.
  '''
  def get_sockbuf( self, sock ):
    return sock.getsockopt( socket.SOL_SOCKET, socket.SO_SNDBUF )

  def set_sockbuf( self, sock, size ):
    size_pre = self.get_sockbuf( sock )
    sock.setsockopt( socket.SOL_SOCKET, socket.SO_SNDBUF, size )
    sock.setsockopt( socket.SOL_SOCKET, socket.SO_RCVBUF, size )
    size_att = self.get_sockbuf( sock )
    if size_pre == size_att:
      self.alert( "BufSize invariato" )
    else:
      if size != size_att:
        self.alert( "BufSize non impostato come richiesto" )

  def set_objkeysec( self ):
    return {'key': self.data['key'], 'secret': self.data['secret']}

  def set_objkeysectok( self ):
    p = self.set_objkeysec()
    p['token']  = self.onlinedata['token']
    return p 

  def set_objcnn( self, m=MsgPrt.UNSET, p={} ):
    '''
      Oggetto che viene scambiato tra client e server
    '''
    return {'msg': m, 'payload': p}

  def get_objmsg( self, obj ):
    return obj['msg']

  def get_objpayload( self, obj ):
    return obj['payload']

  def sendall( self, sock, objdata ):
    # Invio di dati
    delim = "\x1a\x1a\x1a\x1a\x1a"
    try:
      self.debug( ">>> '{0}'".format( objdata ) )
      z_obj = self.zipobj( objdata ) 
      z_obj = z_obj + delim
      sock.sendall( z_obj )
      self.debug( "{0:.2f} KB data sent".format( len( z_obj ) / 1024.0 ) )
    except Exception as e:
      self.printErr( Err=e, Mess="" )
      raise

  def recv( self, sock ):
    # Ricezione di dati
    delim = "\x1a\x1a\x1a\x1a\x1a"
    ld    = len(delim)
    try:
      z_data = ""
      # Eseguo recv fino a ricevere il terminatore delim
      while True:
        rec = sock.recv( self.bufsize )
        l   = len( rec )
        if rec[ l-ld : l ] == delim:
          # Se trovo terminatore aggiungo solo la parte iniziale di rec
          z_data = z_data + rec[ 0 : l-ld ]
          break
        else:
          # Aggiungo rec
          z_data = z_data + rec
      self.debug( "{0:.2f} KB data recv".format( len( z_data ) / 1024.0 ) )

      data    = self.unzipobj( z_data )
      self.debug( "<<< '{0}'".format( data ) )
      return data
    except Exception as e:
      self.debug( z_data )
      self.printErr( Err=e, Mess="" )
      raise

  def sendrecv( self, sock, objdata ):
    # Invio dati al peer ed attesa della risposta.
    try:

      ret  = self.sendall( sock, objdata )
      data = self.recv( sock )
      return data
      
    except Exception as e:
      self.printErr( Err=e, Mess="" )
      raise

  ''' -------------------------------------------------------------------------
    Metodi datafile
    
    Tutti questi metodi sollevano eccezioni che vanno gestite da chi li chiama.
  '''
  def set_datafile( self, filename='' ):
    '''
      Imposta il nomefile e fa partire il thread di salvataggio automatico
    '''
    self.data['io']['fname'] = filename

  def read( self ):
    '''
      Legge la struttura dati self.data e self.jobs da file
        Poiche' potrebbe essere molto grande e ci potrebbe mettere tempo a leggere, unzippare
        e deserializzare la struttura dati, si serve del flag self.data['io']['reading'], se all' inizio lo trova False
        effettua la lettura, altrimenti significa che stava gia' salvando.
    '''
    if self.data['io']['fname'] != '':
      # Evito concorrenza con eventuale lettura/scrittura
      while (self.data['io']['reading'] or self.data['io']['writing']) == True:
        self.debug( "attendo fine lettura concorrente" )
        time.sleep(5)
        pass
      if self.data['io']['reading'] == False:
        # Flag inizio lettura
        self.data['io']['reading'] = True
        try:
          self.alert( "Inizio lettura datafile" )
          fname = "{0}.data".format( self.data['io']['fname'] )
          self.debug( "Leggo {0}". format( fname ) )
          file      = open( fname, 'r' )
          zipdata   = file.read()
          file.close()
          data = self.unzipobj( zipdata )
          # Validazione dati
          if 'version' not in data:
            raise E_MissVer
          if data['version'] != self.version:
            raise E_WrongVer( data['version'] )
          if 'io' in data:
            if type( data['io'] ) != dict:
              raise E_DataCorrupt
            # Inizializzo i flag
            data['io']['writing'] = False      # Quando salvo questo flag rimane True perche' viene resettato a scrittura avvenuta
            data['io']['changed'] = False      # Quando salvo questo flag rimane True perche' viene resettato a scrittura avvenuta
            data['io']['load']    = True
          else:
            data['io'] = self.io_create()

          # Dati consistenti 
          self.data = data

          # Reset flag
          for jid, jdata in self.data['jobs_io'].items():
            if ( jdata['load'] or jdata['changed'] or jdata['error'] or jdata['reading'] or jdata['writing'] or jdata['unloading'] ) == True:
              jdata['load']              = False
              jdata['changed']           = False
              jdata['reading']           = False
              jdata['writing']           = False
              jdata['unloading']         = False
              jdata['error']             = False
              self.data['io']['changed'] = True
          
          # Lettura data file dei jobs
          # Leggo prima tutti i job che erano WORKING poi se avanza spazio quelli NEW
          j_read = 0
          if 'jobs_io' in self.data:
            for jid, jdata in self.data['jobs_io'].items():
              if jid not in self.jobs and jdata['status'] == JobStatus.WORKING:
                jdata['thread'] = threading.Thread( name=jdata['fname'], target=self.jobread, args=( jid, jdata, ) )
                jdata['thread'].start()
                j_read += 1
          if j_read < self.data['io']['maxdatafile']:
            for jid, jdata in self.data['jobs_io'].items():
              if jid not in self.jobs and jdata['status'] == JobStatus.NEW:
                jdata['thread'] = threading.Thread( name=jdata['fname'], target=self.jobread, args=( jid, jdata, ) )
                jdata['thread'].start()
                j_read += 1
              if j_read > self.data['io']['maxdatafile']:
                break
            
          # Attendo lettura di tutti i job datafile  
          for jid, jdata in self.data['jobs_io'].items():
            if jdata['thread'] != None:
              jdata['thread'].join()
              jdata['thread'] = None
             
          self.alert( "Fine lettura datafile" )

        except Exception as e:
          self.printErr( Err=e, Mess="" )
          self.data['io']['reading'] = False
          raise
        finally:
          self.data['io']['reading'] = False

    else:
      raise E_FileName

  def write( self ):
    '''
      Salva su file la struttura dati self.data e self.jobs
        Poiche' potrebbe essere molto grande e ci potrebbe mettere tempo a serializzare, zippare
        e salvare la struttura dati, si serve del flag self.data['io']['writing'], se all' inizio lo trova False
        effettua il salvataggio, altrimenti significa che stava gia' salvando.
      Viene anche richiamata dal loop del thread __start_autosave.
      Per attivare i salvataggi bisogna valorizzare self.data['io']['fname'] e impostare self.data['io']['changed'] True
      I salvataggi vengono effettuati automaticamente ogni self.autosave_timeout secondi
    '''
    if self.data['io']['fname'] != '':
      # Evito concorrenza con eventuale lettura/scrittura
      while (self.data['io']['reading'] or self.data['io']['writing']) == True:
        self.debug( "attendo fine scrittura concorrente" )
        time.sleep(5)
        pass
      if self.data['io']['writing'] == False:
        # Flag inizio scrittura
        self.data['io']['writing'] = True
        # Uniformazione self.data
        if 'version' not in self.data:
          self.data['version'] = self.version
        # Scrivo il datafile
        try:
          self.alert( "Inizio scrittura datafile" )
          fname = "{0}.data".format( self.data['io']['fname'] )
          self.debug( "Scrivo {0}".format( fname ) )
          zipdata = self.zipobj( self.data )
          file    = open( fname, 'w' )
          file.write( zipdata )
          file.flush()
          file.close()

          if 'jobs_io' in self.data:
            for jid, jdata in self.data['jobs_io'].items():
              if jdata['changed']:
                jdata['thread'] = threading.Thread( name=jdata['fname'], target=self.jobwrite, args=( jid, jdata, ) )
                jdata['thread'].start()

          # Attendo scrittura di tutti i job datafile
          for jid, jdata in self.data['jobs_io'].items():
            if jdata['thread'] != None:
              jdata['thread'].join()
              jdata['thread'] = None
 
          self.alert( "Fine scrittura datafile" )
        except Exception as e:
          self.printErr( Err=e, Mess="" )
        finally:
          self.data['io']['changed'] = False
          self.data['io']['writing'] = False
    else:
      raise E_FileName

  '''
    Thread autosave
  '''

  def __start_autosave( self ):
    '''
      Questo metodo viene eseguito in un thread separato all'interno di start_autosave
      Si occupa di eseguire il salvataggio automatico dei dati chiamando il metodo self.write
      quando ci sono flag changed True nelle strutture dati o alla scadenza di 
      self.autosave_timeout.
    '''
    self.loop_autosave = True
    self.autosaving    = True
    while self.loop_autosave:
      # Autosalvataggio ogni tot
      now                = time.time()
      b_to_exp           = ( ( self.autosave_last + self.autosave_timeout ) < now )
      self.autosave_last = now

      # Salvataggio dati
      if ( self.data['io']['changed'] == True ) or b_to_exp:
        try:
          self.write()
        except Exception as e:
          self.printErr( Err=e, Mess="" )
      time.sleep( 10 )

    self.alert( "Stop loop auto save" )
    self.autosaving = False

  def start_autosave( self, p_timeout=600 ):
    '''
      Fa partire il thread dell' autosave.
      Va lanciato implicitamente
    '''
    self.autosave_timeout = p_timeout
    if self.th_autosave == None:
      self.alert( "Avvio autosave" )
      try:
        self.th_autosave = threading.Thread( name='Autosave', target=self.__start_autosave, args=() )
        self.th_autosave.setDaemon( True )
        self.th_autosave.start()
      except Exception as e:
        self.printErr( Err=e, Mess="" )
    else:
      self.alert( "Autosave attivo ogni {0} s".format( p_timeout ) )

  def __stop_autosave( self ):
    '''
      Fa terminare il thread dell' autosave
    '''
    self.debug( "Richiesta stop autosave" )
    self.loop_autosave    = False
    if self.th_autosave != None:
      self.th_autosave.join()
      self.th_autosave = None
    self.debug( "Stop autosave" )

  def stop_autosave( self ):
    self.__stop_autosave()

  '''
  # Metodi jobs e task
  '''
  def io_create( self ):
    return { 'writing': False, 'changed': False, 'load': True }

  def jobio_create( self, fname ):
    '''
      fname:        nome datafile
      writing:      flag scrittura in corso
      reading:      flag lettura in corso
      unloading:    flag di scaricamento del datafile dalla memoria in corso
      changed:      flag dati modificati, necessario salvataggio
      load:         flag datafile caricato e presente in self.jobs
      error:        flag segnalazione di errore
      thread:       riferimento al thread che sta gestendo l'operazione di lettura / scrittura
      status:       stato del job memorizzato nel datafile
    '''
    return {'fname': fname, 'writing': False, 'reading': False, 'unloading': False, 'changed': True, 'load': False, 'error': False, 'thread': None, 'status': JobStatus.UNSET }

  def _showjob( self, j ):
    f = Color.GREEN + "JobId: {0:>6d} Data: {1:>40}" + Color.RESET
    print( f.format( j['id'], j['data'] ) )

    f = Color.GREEN + "Desc: {0:>40s}" + Color.RESET
    print( f.format( j['desc'] ) )

    f = Color.GREEN + "State: {0:>6s} Taks: {1:>8d}/{2:>8d} Compl: {3:>.2f}%" + Color.RESET
    print( f.format( ['New','Work','Done','Err'][ j['status'] ], j['task_done'], j['task_num'], 100.0*j['task_done']/j['task_num'] ) )

    f = Color.GREEN + "Tempo elaborazione Job: {0:>8.2f} s Tempo elaborazione tasks: {1:>8.2f} s" + Color.RESET
    print( f.format( j['received'] - j['sent'], j['el_time'] ) )

  def showjobs( self, joblist={} ):
    if joblist == {}:
      joblist = self.jobs 
    if len( joblist ) > 0:
      for jid, j in joblist.items():
        self._showjob( j )

  def showtasks( self, joblist={}, w_status=None ):
    if joblist == {}:
      joblist = self.jobs 
    if len( joblist ) > 0:
      for jid, j in joblist.items():
        self._showjob( j )
        if len( j['tasks'] ) >  0 :
          f = "    {0:>6s} {1:>6s} {2:>30s} {3:>12s} {4:>20s}"
          print( f.format( 'TaskId', 'State', 'Data', 'Res', 'Token' ) )
          for idt, t in j['tasks'].items():
            if w_status == None or w_status == t['status']:
              f = "    {0:>6d} {1:>6s} {2:>30s} {3:>12s} {4:>20s}"
              print( f.format( t['id'], ['New','Work','Done','Err'][ t['status'] ], t['data'], t['result'], t['token'] ) )

  def jobcreate( self, data=[], desc='', ffile='' ):
    # Cerco il file contenente la funzione taskcompute da inviare ai client
    b_valid = False
    if ffile != '':
      try:
        f  = open( ffile, "r" )
        fn = f.read()
        f.close()
        # Eseguo test sul codice letto
        # Migliorare la ricerca usando regex
        if fn.find( "taskcompute" ) < 0:
          self.alert( "Non ho trovato taskcompute" )
        b_valid = True
      except Exception as e:
        self.printErr( Err=e, Mess="File taskcompute" )
    if b_valid == False:
      fn = "def taskcompute( task={} ):\n  print task\n  return task['data']"

    # Inizializzo una struttura dati job
    j = {}
    j['id']        = -1     # Assegno l'id quando aggiungo il job al dictionary, in questo modo riconosco job appena creati
    j['data']      = data   # Lista di valori utili da ricordare
    j['desc']      = desc   # Descrizione
    j['task_num']  = 0      # Numero di task contenuti
    j['task_done'] = 0      # Numero di task completati
    j['status']    = JobStatus.NEW
    j['func']      = zlib.compress( fn )
    j['token']     = ''     # token del client a cui e' assegnato per l'elaborazione
    j['sent']      = 0      # time invio
    j['received']  = 0      # time ricezione
    j['el_time']   = 0      # somma dei tempi di elaborazione di tutti i client >> el_end - el_start
    j['tasks']     = {}     # dictionary di task
    return j

  def jobget( self, jobid=-1 ):
    if jobid != -1:
      try:
        return self.jobs[ jobid ] 
      except Exception as e:
        self.printErr( Err=e )
        raise

  def jobadd( self, j ):
    '''
      Aggiunge il job a self.data['jobs_io'] e self.jobs
      Se ci sono troppi jobs caricati su self.jobs procede a scaricare il datafile
    '''
    # j: job { .... }

    # Calcolo l'id per i nuovi jobs
    if j['id'] == -1:
      j['id']     =  self.data['next_job_id']
      self.data['next_job_id'] += 1

    # Inserimento
    try:
      jid = j['id']
      if jid not in self.jobs:
        # Aggiungo il job al dictonary con chiave uguale all' id
        self.jobs[ jid ] = j
        if jid not in self.data['jobs_io']:
          fname = "{0}_{1}_{2}.data".format( self.data['io']['fname'], j['desc'], jid )
          self.data['jobs_io'][ jid ] = self.jobio_create( fname )
          # setto lo stato uguale a quello di j['status'] perche' j potrebbe essere un datafile che viene ricaricato
          self.jobsetstatus( jid, j['status'] )
          self.data['jobs_io'][ jid ]['load'] = True
          # Se ci sono gia' troppi job online scarico il datafile di questo appena aggiunto
          if len( self.jobs ) > self.data['io']['maxdatafile']:
            self.jobunload( jid )
            
      else:
        # JobId presente
        raise Exception("Il JobId {0} e' gia' presente e non puo' essere duplicato.".format( j['id'] ) )
    except Exception as e:
      self.printErr( Err=e )
      raise

  def jobread( self, jid, jdata ):
    '''
      Legge il datafile per il job jid presente in self.data['jobs_io'] e lo inserisce in self.jobs tramite self.jobsadd
    '''
    jfname = jdata['fname']
    self.debug( "Leggo {0}". format( jfname ) )
    try:
      # Concorrenza lettura/scrittura
      while ( jdata['reading'] or jdata['writing'] ) == True:
        self.debug( "Attendo concorrenza {0} {1} {2}".format( jfname, jdata['reading'], jdata['writing'] ) )
        time.sleep(5)
        pass
      jdata['reading'] = True
      file      = open( jfname, 'r' )
      zipdata   = file.read()
      file.close()
      j = self.unzipobj( zipdata )
      if 'tasks' in j:
        if type( j['tasks'] ) != dict:
          raise E_DataCorrupt
      else:
        j['tasks'] = {}
      # dati job consistenti
      self.jobadd( j )
      jdata['load']    = True
      jdata['error']   = False
    except IOError as e:
      if e.errno == 2:
        # No such file or directory
        self.alert( "Datafile {0} non trovato".format( jfname ) )
        # Rimuovo il riferimento da self.data['jobs_io']
        jdata['error'] = True
        del self.data['jobs_io'][ jid ]
        pass
    except Exception as e:
      self.printErr( Err = e )
      jdata['error']   = True
      raise
    finally:
      jdata['reading'] = False

  def jobwrite( self, jid, jdata ):
    '''
      Salva su disco il datafile contenente i dati self.jobs[ jid ] per il job jid
    '''
    jfname = jdata['fname']
    self.debug( "Scrivo {0}".format( jfname ) )
    try:
      # Concorrenza lettura/scrittura
      while ( jdata['reading'] or jdata['writing'] ) == True:
        pass
      jdata['writing'] = True
      zipdata = self.zipobj( self.jobs[ jid ] )
      file    = open( jfname, 'w' )
      file.write( zipdata )
      file.flush()
      file.close()
      jdata['changed'] = False
      jdata['error']   = False
    except Exception as e:
      self.printErr( Err = e )
      jdata['error']   = True
      raise
    finally:
      jdata['writing'] = False

  def jobunload( self, jid ):
    '''
      Scarica dalla memoria self.jobs il job jid mantenendo il riferimento in self.data['jobs_io']
      Blocca fino al completamento di jobwrite
    '''
    if jid in self.data['jobs_io'] and jid in self.jobs:
      jdata = self.data['jobs_io'][ jid ]
      jdata['unloading'] = True
      if jdata['changed']:
        self.jobwrite( jid, jdata )
      jdata['load'] = False
      del self.jobs[ jid ]
      jdata['unloading'] = False
 
  def jobdelete( self, jid ):
    '''
      Elimina il job da self.data['jobs_io'] e self.jobs
      Blocca fino al completamento di jobwrite
    '''
    if jid in self.data['jobs_io'] and jid in self.jobs:
      jdata = self.data['jobs_id'][ jid ]
      self.jobwrite( jid, jdata )
    if jid in self.data['jobs_io']:
      del self.data['jobs_io'][ jid ]
    if jid in self.jobs:
      del self.jobs[ jid ]

  def jobsetstatus( self, jid, jstatus ):
    '''
      Mantiene allineato lo status del job in self.data['jobs_io'] e self.jobs
    '''
    jdata = self.data['jobs_io'][ jid ]
    jdata['status'] = jstatus
    if jid in self.jobs:
      self.jobs[ jid ]['status'] = jstatus
    jdata['changed'] = True

  def jobonline( self ):
    '''
      Si occupa di scaricare i datafile completati, di tenere un limite di datafile NEW in memoria scaricando quelli di troppo e caricando quelli che mancano
    '''
    th = []

    # Scarico job completati da self.jobs
    kj_all    = self.data['jobs_io'].keys()
    kj_online = self.jobs.keys()                        # Salvo gli id presenti perche' nel frattempo possono essere stati aggiunti o scaricati jobs
    kj_online_not_done = []
    for jid in kj_online:
      jdata = self.data['jobs_io'][ jid ]
      if jid in self.jobs and jdata['status'] == JobStatus.DONE and jdata['unloading'] == False:
        self.alert( "Scarico job DONE {0}".format( jid ) )
        t = threading.Thread( name='jobonline', target=self.jobunload, args=( jid, ) )
        th.append( t )
      if jid in self.jobs and jdata['status'] != JobStatus.DONE:
        kj_online_not_done.append( jid )

    # Controllo numero di datafile NEW da tenere in memoria

    # Compilo la lista di job NEW che non stanno per essere scaricati
    kj_online_not_done.reverse()                        # Parto dal fondo perche' i primi jobs sono quelli che vengono assegnati
    kj_online_new = []
    for jid in kj_online_not_done:
      jdata = self.data['jobs_io'][ jid ]
      if jdata['status'] == JobStatus.NEW and jdata['unloading'] == False:
        kj_online_new.append( jid )

    # in j_new ci sono solo gli id di jobs in stato NEW
    # Se sono troppi ne scarico num_unload
    num_unload = len( kj_online_new ) - self.data['io']['maxdatafile']
    if num_unload > 0:
      for i in range( num_unload ):
        jid   = kj_online_new[ i ]
        jdata = self.data['jobs_io'][ jid ]
        if jid in self.jobs and jdata['status'] == JobStatus.NEW and ( jdata['unloading'] or  jdata['reading'] ) == False:
          self.alert( "Scarico job NEW {0}".format( jid ) )
          t = threading.Thread( name='jobonline', target=self.jobunload, args=( jid, ) )
          th.append( t )
          
    # Se sono pochi ne carico num_read prelevando gli id da self.data['jobs_io']
    num_read   = self.data['io']['maxdatafile'] - len( kj_online_new )
    if num_read > 0:
      for jid in kj_all:
        jdata = self.data['jobs_io'][ jid ]
        if jid not in self.jobs and jdata['status'] == JobStatus.NEW and ( jdata['unloading'] or  jdata['reading'] ) == False:
          self.alert( "Carico job NEW {0}".format( jid ) )
          t = threading.Thread( name='jobonline', target=self.jobread, args=( jid, jdata, ) )
          th.append( t )
          num_read -= 1
        if num_read == 0:
          break
      
    # Start threads
    for t in th:
      t.start()
      
    # Attesa fine dei threads
    for t in th:
      if t.isAlive():
        t.join()

  def taskcreate( self, data=[] ):
    # Inizializzo una struttura dati task
    t = {}
    t['id']      = -1     # Assegno l'id quando aggiungo il task ad un job, in questo modo riconosco task appena creati
    t['data']    = data   # dati da elaborare
    t['result']  = []     # risultato dell' elaborazione
    t['status']  = TaskStatus.NEW
    t['start']   = 0      # time inizio elaborazione
    t['end']     = 0      # time fine elaborazione
    return t

  def taskadd( self, j=None, t=None ):
    '''
      Aggiunge il task al job
    '''
    # j e' un job { ... }
    # t e' un task { ... }

    if ( j is not None and t is not None ):

      # Controllo che si possano aggiungere task
      if j['task_num'] == self.max_tasks_per_job:
        raise E_MaxTaskPerJob

      if t['id'] == -1:
        t['id']     = self.data['next_task_id']
        self.data['next_task_id'] += 1
      # Inserimento
      try:
        tid = t['id']
        if tid not in j['tasks']:
          # Aggiungo il task al job
          j['tasks'][ tid ] = t
          j['task_num'] += 1
        else:
          # TaskId presente
          raise Exception("Il Task {0} e' gia' presente nel Job {1} e non puo' essere duplicato.".format( t['id'], j['id'] ) )
      except Exception as e:
        self.printErr( Err=e )
        raise

# def restarttask( ... ):
# def restartjob( ... ):
# def restartjobs( ... ):

  '''
    Esecuzione callback generiche
  '''

  def exec_call_back( self, f, *args, **kwargs ):
    if f != None:
      f( *args )

#
# Client Class
#  
     
class Client( PyCalDis ):

  def __init__( self, host='127.0.0.1', port=4096 ):
    super( Client, self ).__init__( host=host, port=port )

    # Lettura sessione precedente da datafile
    self.set_datafile( 'client' )
    try:
      self.read()
    except Exception as e:
      self.printErr( Err=e, Mess="" )

    # Inizializzazione

    self.onlinedata['token']  = ''

  def __del__( self ):
    super( Client, self ).__del__()

  #  Metodi base client

  def execconn( self, f_callback, *par, **kwpar ):
    '''
      Incapsulo l'esecuzione di una funzione call back all'interno di startconn ed endconn
        gestendo in un punto solo la chiusura della sock in presenza di errori bloccanti
        all'interno della callback segnalati tramite raise

      f_callback	funzione da eseguire tra startconn ed endconn
    '''
    # startconn
    sock = None
    try:  
      host = ( self.host, self.port )
      sock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

      sock.settimeout( 30 )
      sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
      sock.setsockopt( socket.SOL_TCP, socket.TCP_NODELAY, 1 )

      self.bufsize = self.get_sockbuf( sock )

      sock.connect( host )
    except Exception as e:
      #self.printErr( Err=e, Mess="" )
      raise

    if sock != None:
      try:
        f_callback( sock, *par )
      except Exception as e:
        # Errori di invio / ricezione
        self.printErr( Err = e )
        raise
      finally:
        # Chiudo il socket self.sock
        #   TODO Testare cosa succede in caso di raise su except precedente
        try:
          sock.shutdown( socket.SHUT_RDWR )
        except socket.error as e:
          if e.errno == 9:
            pass
        except:
          raise
        finally:
          sock.close()


  def frecall( self, times, f, *par, **kwpar ):
    '''
      Esegue una funzione generica con i relativi parametri.
        Se la funzione solleva eccezione E_Recall riesegue la funzione.
        In presenza di altre eccezioni esce.
        Se non ci sono eccezioni esce sollevando E_RecallStop
        Le funzioni chiamate con frecall devono terminare l'esecuzione con raise E_RecallStop

        def f( n ):
          print n
          if EseguiAncora:
            raise E_RecallAgain

          raise E_RecallStop

        try:
          self.frecall( 5, f, 0 )
        except E_RecallStop:
          pass
        except E_RecallMax:
          raise
        except Exception:
          raise
    '''
    recall = times
    while recall > 0:
      try:
        f( *par )
        break
      except E_RecallStop as e:
        break
      except E_RecallAgain as e:
        self.alert("Riprovo...")
        recall -= 1
        time.sleep( 5 )
      except E_ExecError as e:
        raise
      except Exception as e:
        raise

    if recall == 0:
      raise E_RecallMax
    else:
      raise E_RecallStop

  #  Metodi Client

  '''
    callback richiamate da frecall
  '''

  def __send_ack( self, sock, s_success_alert ):
    # Invio ACK
    # TODO: ACK non invia credenziali key e sec?
    obj = self.set_objcnn( MsgPrt.ACK )
    try:
      rec = self.sendall( sock, obj )
      self.alert( s_success_alert )

    except Exception as e:
      # errore invio ACK
      #self.printErr( Err=e, Mess="" )
      raise E_RecallAgain

    raise E_RecallStop

  def __send_signup( self, sock ):
    # Invio SIGNUP e ricezione SIGNUP_OK
    obj = self.set_objcnn( MsgPrt.SIGNUP )

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
 
      if m == MsgPrt.SIGNUP_OK:

        rp = self.get_objpayload( rec )
        self.data['clientname'] = rp['clientname']
        self.data['created']    = rp['created']
        self.data['key']        = rp['key']
        self.data['secret']     = rp['secret']

        try:
          self.write()
        except Exception as e:
          # Errore write su file, non blocco flusso
          self.printErr( Err=e, Mess="" )
  
        try:
          s_success_alert = "Registrato: {0}".format( rp['clientname'] )
          self.frecall( 5, self.__send_ack, sock, s_success_alert )
        except E_RecallStop:
          pass
        except E_RecallMax:
          raise
        except Exception as e:
          raise

    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  def __send_login( self, sock ):
    # Invio LOGIN e ricezione LOGIN_OK 
    obj = self.set_objcnn( MsgPrt.LOGIN, self.set_objkeysec() )

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
      if m == MsgPrt.LOGIN_OK:

        rp = self.get_objpayload( rec )
        self.onlinedata['token'] = rp['token']
       
        # Invio ACK 
        try:
          s_success_alert = "Ricevuto Token: {0}".format( rp['token'] )
          self.frecall( 5, self.__send_ack, sock, s_success_alert )
        except E_RecallStop:
          pass
        except E_RecallMax:
          raise
        except Exception as e:
          raise

    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  def __send_adm_debug_flags( self, sock, f_flag ):
    # Invio ADM_DEBUG e ricezione ACK
    p   = self.set_objkeysec()
    p['flag'] = f_flag
    obj = self.set_objcnn( MsgPrt.ADM_DEBUG, p )

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
      if m == MsgPrt.NOT_ADMIN:
        raise E_NotAdmin
      if m == MsgPrt.ADM_DEBUG_OK:
        rp = self.get_objpayload( rec )
        self.alert( "Flags: debug: {0} alert: {1} errors: {2}".format( rp['f_debug'], rp['f_alert'], rp['f_errors']) )

    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  def __send_adm_client_db( self, sock ):
    # Invio ADM_CLIENT_DB      
    p   = self.set_objkeysec()
    obj = self.set_objcnn( MsgPrt.ADM_CLIENT_DB, p )

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
      if m == MsgPrt.NOT_ADMIN:
        raise E_NotAdmin
      if m == MsgPrt.ADM_CLIENT_DB_OK:
        p = self.get_objpayload( rec )
        for client in p:
          print ""
          for k in client:
            print ( Color.GREEN + "{0:<8s}" + Color.RESET + " {1}" ).format(k, client[k])

    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  def __send_adm_client_online( self, sock ):
    # Invio ADM_CLIENT_ONLINE
    p   = self.set_objkeysec()
    obj = self.set_objcnn( MsgPrt.ADM_CLIENT_ONLINE, p )

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
      if m == MsgPrt.NOT_ADMIN:
        raise E_NotAdmin
      if m == MsgPrt.ADM_CLIENT_ONLINE_OK:
        p = self.get_objpayload( rec )
        for client in p:
          print ""
          for k in client:
            print (  Color.GREEN + "{0:<8s}" + Color.RESET + " {1}" ).format(k, client[k])

    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  def __send_request( self, sock ):
    # Invio JOBREQUEST 
    p   = self.set_objkeysectok()
    obj = self.set_objcnn( MsgPrt.JOBREQUEST, p ) 

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
      if m == MsgPrt.JOBREQUEST_CALC:
        pl = self.get_objpayload( rec )
        for jid, j in pl['data'].items():
          try:
            self.jobadd( j )
            # Rispondo OK
            obj = self.set_objcnn( MsgPrt.ACK, p )
            rec = self.sendall( sock, obj )
            raise E_RecallStop
          except Exception as e:
            self.printErr( Err=e )
            # Segnalo al server errore
            obj = self.set_objcnn( MsgPrt.ERROR, p )
            rec = self.sendall( sock, obj )
            raise E_ExecError
      if m == MsgPrt.CREDNOTVALD:
        self.alert( "Credenziali non valide" )
        raise E_CredNonVald
      if m == MsgPrt.NOT_LOGGED:
        self.alert( "Prima bisogna fare login" )
        raise E_NoLogin
      if m == MsgPrt.JOBREQUEST_NOTHING:
        self.alert( "Niente da fare" )
        raise E_NothingToDo

    except zlib.error as e:
      '''
        Errore decompressione di zlib:
          Error -5 while decompressing data: incomplete or truncated stream
      '''
      obj = self.set_objcnn( MsgPrt.ERROR, p )
      rec = self.sendall( sock, obj )
      raise E_ExecError
    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  def __send_jobdone( self, sock ):
    # Invio JOBDONE
    p   = self.set_objkeysectok()
    # Passo al server il dictionary con i job elaborati
    p['data']   = self.jobs
    obj = self.set_objcnn( MsgPrt.JOBDONE, p ) 

    try:
      rec = self.sendrecv( sock, obj )
      m   = self.get_objmsg( rec )
      if m == MsgPrt.ERROR:
        raise E_ExecError
      if m == MsgPrt.UNEXPECTED:
        raise E_MsgUnexpected
      if m == MsgPrt.RESEND:
        raise E_RecallAgain
      if m == MsgPrt.ACK:
        # Il server ha ricevuto, Cancello i job
        for jid, j in self.jobs.items():
          for tid, t in j['tasks'].items():
            del t
          del j
          del self.data['jobs_io'][ jid ]
        self.jobs            = {}
        self.data['jobs_io'] = {}
      if m == MsgPrt.CREDNOTVALD:
        self.alert( "Credenziali non valide" )
        raise E_CredNonVald 
      if m == MsgPrt.NOT_LOGGED:
        self.alert( "Prima bisogna fare login" )
        raise E_NoLogin

    except Exception as e:
      raise E_RecallAgain

    raise E_RecallStop

  '''
    callback richiamate da execconn
      Il primo parametro e' sempre sock
      Possono avere altri parametri dopo sock
  '''

  def __signup( self, sock ):
    # Riesegue invio SIGNUP
    try:
      self.frecall( 5, self.__send_signup, sock )
    except E_RecallStop:
      pass
    except:
      raise

  def __login( self, sock ):
    # Riesegue invio LOGIN
    try:
      self.frecall( 5, self.__send_login, sock )
    except E_RecallStop:
      pass
    except:
      raise

  def __serverdebug( self, sock, f_flag ):
    # Riesegue invio LOGIN
    try:
      self.frecall( 5, self.__send_adm_debug_flags, sock, f_flag )
    except E_RecallStop:
      pass
    except:
      raise

  def __clientdb( self, sock ):
    # Invio ADM_CLIENT_DB
    try:
      self.frecall( 5, self.__send_adm_client_db, sock )
    except E_RecallStop:
      pass
    except:
      raise

  def __clientonline( self, sock ):
    # Invio ADM_CLIENT_ONLINE
    try:
      self.frecall( 5, self.__send_adm_client_online, sock )
    except E_RecallStop:
      pass
    except:
      raise

  def __jobrequest( self, sock ):
    # Invio JOBREQUEST 
    try:
      self.frecall( 5, self.__send_request, sock )
    except E_RecallStop as e:
      pass
    except:
      raise

  def __jobdone( self, sock ):
    # Invio JOBDONE
    try:
      self.frecall( 5, self.__send_jobdone, sock )
    except E_RecallStop:
      pass
    except:
      raise

  '''
    Metodi che utilizzano execconn
  '''
 
  def Signup( self ):
    '''
    Presentazione nuovo client al server e ricezione credenziali identificative
      clientname
      created
      key
      secret
    '''
    try:  
      self.execconn( self.__signup ) 
    except Exception as e:
      raise

  def Login( self ):
    '''
      Invio credenziali ed inizio nuova sessione.
        Ricezione token
    '''
    try:  
      self.execconn( self.__login ) 
    except Exception as e:
      raise
       
  def ServerToggleDebug( self ):
    '''
      Debug remoto
      Da implementare
    '''
    try:  
      self.execconn( self.__serverdebug, 'f_debug' ) 
    except Exception as e:
      raise
    
  def ServerToggleAlert( self ):
    '''
      Debug remoto
      Da implementare
    '''
    try:  
      self.execconn( self.__serverdebug, 'f_alert' ) 
    except Exception as e:
      raise
    
  def ServerToggleError( self ):
    '''
      Debug remoto
      Da implementare
    '''
    try:  
      self.execconn( self.__serverdebug, 'f_errors' ) 
    except Exception as e:
      raise

  def ClientDb( self ):
    '''
      Elenco client registrati
    '''
    try:  
      self.execconn( self.__clientdb ) 
    except Exception as e:
      raise
  
  def ClientOnline( self ):
    '''
      Elenco client online
    '''
    try:  
      self.execconn( self.__clientonline ) 
    except Exception as e:
      raise

  def JobRequest( self ):
    '''
      Richiesta job con i task da eseguire
    '''
    try:  
      self.execconn( self.__jobrequest ) 
    except E_ExecError as e:
      raise
    except Exception as e:
      raise

  def JobDone( self ):
    '''
      Invio job elaborati
    '''
    try:  
      self.execconn( self.__jobdone ) 
    except Exception as e:
      raise

  '''
    Elaborazione task
  '''

  def Compute( self ):
    for jid, j in self.jobs.items():
      if j['status'] in [JobStatus.NEW, JobStatus.WORKING]:
        # j['func'] contiene il codice zippato che implementa la
        #   funzione taskcompute da eseguire

        # TODO: testare esistenza file, parametrizzare nome funzione importata
        #   per renderla univocamente determinata dal job

        func = zlib.decompress( j['func'] )
        mn   = "j{0}".format( j['id'] ) # module name
        fn   = "{0}.py".format( mn )    # file name
        
        f    = open( fn, "w" )
        m    = None
        try:
          f.write( func )
          f.flush()		# Fondamentale, altrimenti import trova un file vuoto
          f.close
        except Exception as e:
          self.printErr( Err=e, Mess="" )

        try:
          m = importlib.import_module( mn )
        except Exception as e:
          self.printErr( Err=e, Mess="" )
       
        if 'taskcompute' in dir(m):
          for tid, t in j['tasks'].items():
            if t['status'] in [TaskStatus.NEW, TaskStatus.WORKING]:
              self.jobsetstatus( j['id'], JobStatus.WORKING )
              t['start']      = time.time()
  
              r = m.taskcompute( t )
  
              t['end']        = time.time()
              t['status']     = TaskStatus.DONE
              t['result']     = r
              j['task_done'] += 1

          self.jobsetstatus( j['id'], JobStatus.DONE )
          self.data['jobs_io'][ jid ]['changed'] = True

        try:
          fn = "{0}.py".format( mn )
          if os.path.isfile( fn ):
            os.remove( fn )
          fn = "{0}.pyc".format( mn )
          if os.path.isfile( fn ):
            os.remove( fn )
        except Exception as e:
          self.printErr( Err=e, Mess="Rimozione file" )
#
# Server Class
#  

class Server( PyCalDis ):

  def __init__( self, port=4096, task_per_job=1024, max_data_file=16 ):
    super( Server, self ).__init__( host='', port=port, max_data_file=max_data_file )
 
    # Lettura sessione precedente da datafile   
    self.set_datafile( 'server' )
    try:
      self.read()
    except Exception as e:
      self.printErr( Err=e, Mess="" )
    
    # config

    # Clients
    if 'clientdb' not in self.data:
      self.data['clientdb']      = []
      self.data['io']['changed'] = True
    
    self.clients                           = 0

    # Tokens
    
    self.onlinedata['tokens']              = []

    # Socks
    self.sock                              = None

    # Threads
    self.th_internalcheck                  = None
    self.th_listen                         = None

    # Callback metodi pubblici
    self.cb_jobdone_stats		   = None        # func( job_ricevuto, task_ricevuto )

    # Dictionary contenente i metodi dinamici che possono essere aggiunti da classi derivate
    #   self.dynamic[ key ] = function
    self.dynamic                           = {}

    # Assegno metodi richiamati dinamicamente all'interno di self.__connected( ... )
    self.dynamic[MsgPrt.ADM_DEBUG]         = self.dyn_ADM_DEBUG
    self.dynamic[MsgPrt.ADM_CLIENT_DB]     = self.dyn_ADM_CLIENT_DB
    self.dynamic[MsgPrt.ADM_CLIENT_ONLINE] = self.dyn_ADM_CLIENT_ONLINE
    self.dynamic[MsgPrt.SIGNUP]            = self.dyn_SIGNUP
    self.dynamic[MsgPrt.LOGIN]             = self.dyn_LOGIN
 
    # Aggiungo metodi richiamati dinamicamente
    self.dynamic[MsgPrt.JOBREQUEST]        = self.dyn_JOBREQUEST
    self.dynamic[MsgPrt.JOBDONE]           = self.dyn_JOBDONE
    
    # Inizializzazione listening del server
    self.serverlistening                   = False
    self.loop_serverlisten                 = False
    
    # Inizializzazione internal check del server
    self.internalchecking                  = False
    self.loop_internalcheck                = False
    
    # jobs
    self.max_tasks_per_job                 = task_per_job

  def __del__( self ):
    super( Server, self ).__del__()

    self.__stop_internalcheck()   
    self.__stop_listen()
    self.stop_autosave() 
    self.write()

  '''
    Tutte le richieste di invio ( sendall ) e ricezione ( recv ) dati vengono effettuate tranmite i metodi
    del socket conn che viene creato da accept all'interno di __listen e poi viene passato in cascata
    come parametro a tutte le chiamate successive.
  '''
 
  # Metodi pubblici
 
  # Start / Stop server
   
  def Listen( self ):
    '''
      Mette il server in ascolto
    '''
    self.__start_listen()

  def Shutdown( self ):
    '''
      Termina il thread __listen
    '''
    self.__stop_listen()

  def Exit( self ):
    '''
      Esegue lo shutdown e l'arresto di tutti i thread.
        E' inutile eseguire anche Shutdown
    '''
    self.__stop_internalcheck()   
    self.__stop_listen()
    self.stop_autosave() 
    self.write()

  #  Metodi interni

  '''
    Thread Internalcheck
  '''

  def __internalcheck( self, loop_internalcheck = True ):
    '''
      Questo metodo viene eseguito in un thread separato all'interno di __init__
      Si occupa di eseguire i controlli periodici che mantengono i dati coerenti.
    '''
    self.loop_internalcheck = loop_internalcheck 
    self.internalchecking   = True
    while self.loop_internalcheck:
      # Scadenza token
      now   = time.time()
      for x in range( len ( self.onlinedata['tokens'] ) ):
        c = self.onlinedata['tokens'][x]
        if c['expire'] < now:
          del self.onlinedata['tokens'][x]

      # Gestione datafile online
      self.jobonline()

      # Timeout assegnazione job
      now   = time.time()
      for jid, j in self.jobs.items():
        if j['status'] == JobStatus.WORKING:
          timeout = j['sent'] + self.job_assign_timeout
          if timeout < now:
            j['sent']     = 0
            j['received'] = 0
            self.jobsetstatus( jid, JobStatus.NEW )
            self.data['jobs_io'][ jid ]['changed'] = True

      time.sleep( 10 )

    self.alert( "Stop internal check" )
    self.internalchecking = False
  
  def __start_internalcheck( self ):
    '''
      Fa partire thread internalcheck
    '''
    try:
      self.th_internalcheck = threading.Thread( name='InternalCheck', target=self.__internalcheck, args=( True, ) )
      self.th_internalcheck.setDaemon( True )
      self.th_internalcheck.start()
    except Exception as e:
      self.printErr( Err=e, Mess="" )

  def start_internalcheck( self ):
    '''
      internalcheck va fatto partire esplicitamente dopo le operazioni di creazione / lettura job
    '''
    self.__start_internalcheck()

  def __stop_internalcheck( self ):
    '''
      Attende la fine di internalcheck
    '''
    # Chiedo al loop di terminare
    self.loop_internalcheck = False
    if self.th_internalcheck != None:
      self.debug( "Attendo fine di internalcheck" )
      self.th_internalcheck.join()
      self.th_internalcheck = None
      self.debug( "Fine di internalcheck" )

  def stop_internalcheck( self ):
    '''
      stop servizio ineternalcheck
    '''
    self.__stop_internalcheck()

  '''
    Thread Listen
  '''

  def __listen( self, loop_serverlisten = True ):
    '''
      Questo metodo viene eseguito in un thread separato da Listen() ed avvia il loop 
      in cui vengono accettate le richieste di connessione dei clients.
      Per ogni connessione esegue in un thread separato il metodo __connected passando
      il nuovo socket generato conn come parametro.
      Si serve di self.serverlistening per indicare che il loop e' attivo
                  self.loop_serverlisten per attivare/fermare il loop
    '''
    self.loop_serverlisten = loop_serverlisten
    try:
      self.sock     = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
      
      self.sock.settimeout( 30 )
      self.sock.setsockopt( socket.SOL_SOCKET, socket.SO_REUSEADDR, 1 )
      self.sock.setsockopt( socket.SOL_TCP, socket.TCP_NODELAY, 1 )

      self.bufsize = self.get_sockbuf( self.sock )

      self.sock.bind( ( self.host, self.port ) )
      self.sock.listen( 5 )
      self.alert( "Ascolto su porta: {0}".format( self.port ) ) 

      self.serverlistening   = True
      while self.loop_serverlisten:
        conn  = None
        try:
          self.alert( "Accetto connessioni" ) 
          conn, addr = self.sock.accept( )
          # Da questo momento i due peer comunicheranno tramite il socker conn
          try:
            t = threading.Thread( name='WORKER', target=self.__connected, args=( conn, addr, ) )
            t.setDaemon( True )
            t.start()
          except Exception as e:
            self.printErr( Err=e, Mess="Non ho potuto create thread __connected"  )
        except Exception as e:
          '''
            Quando il server e' in shutdown eseguo self.sock.close dopo aver impostato self.loop_serverlisten = False
            e self.sock.accept solleva errore 22
          '''
          if self.loop_serverlisten == False:
            # Eccezione sollevata dal server in shutdown
            pass
          else:
            self.printErr( Err=e, Mess="" ) 

      self.alert( "Server offline" ) 
      self.serverlistening   = False
      
    except Exception as e:
      self.printErr( Err=e, Mess="" )
      self.serverlistening   = False

  def __start_listen( self ):
    self.th_listen = None
    try:
      self.th_listen = threading.Thread( name='Listen', target=self.__listen, args=( True, ) )
      self.th_listen.setDaemon( True )
      self.th_listen.start()
    except Exception as e:
      self.printErr( Err=e, Mess="" )
      self.th_listen = None

  def __stop_listen( self ):
    if self.sock != None:
      self.alert( "Shutdown" )

      # Chiedo al loop di terminare
      self.loop_serverlisten  = False

      # Chiudo il socket self.sock generando un' eccezione in __listen
      try:
        self.sock.shutdown( socket.SHUT_RDWR )
      except socket.error as e:
        if e.errno == 9:
          pass
      except:
        raise
      finally:
        self.sock.close()

      # Attando la fine del ciclo Listen
      if self.th_listen != None:
        self.debug( "Attendo fine di listen" )
        self.th_listen.join()
        self.th_listen = None
        self.debug( "Fine listen" )

    else:
      self.alert( "Il server non era in ascolto" )

    # Attesa chiusura di tutti i socks
    te = threading.enumerate()
    for th in te:
      if th.getName() == 'WORKER':
        self.alert( "Attesa fine worker" )
        th.join( 5.0 )

    self.alert( "Server spento" )

  def __connected( self, conn, addr ):
    '''
    Questo metodo viene eseguito in un un thread separato ad ogni richiesta di connessione
      all' interno di __listen.
      Si occupa di ricevere i dati ed eseguire il metodo richieisto dal campo 'msg'
    '''
    # TODO: Rimettere i lock
    self.clients += 1
    
    self.alert( "Connesso con: {0}".format( addr ) ) 
    
    # Ricezione dati, unzip e de-serializzazione
    data = self.recv( conn )

    # self.dynamic contiene la mappatura 'msg' -> self.metodo, cosi' e' possibile
    #   chiamare dinamicamente un metodo tramite self.dynamic[data['msg']](conn, data)
    d_func   = self.get_objmsg( data )
    d_payl   = self.get_objpayload( data )

    if d_func in self.dynamic:
      d_client = None
      if 'key' in d_payl and 'secret' in d_payl:
        d_key    = d_payl['key']
        d_secret = d_payl['secret']
        d_client = self.getClient( d_key, d_secret )
      if d_client != None:
        s_client = d_client['clientname']
      else:
        s_client = "NUOVO"

      self.alert( "{0} >>> {1}".format( s_client, d_func ) )
      self.dynamic[ d_func ](conn, data)
    else:
      self.debug( "Operazione {0} non riconosciuta".format( d_func ) )

    self.alert( "Closing: {0}".format( addr ) ) 
    try:
      conn.shutdown( socket.SHUT_RDWR )
    except socket.error as e:
      if e.errno == 9:
        pass
    except:
      raise
    finally:
      conn.close()
      conn = None
      
    self.alert( "Close: {0}".format( addr ) ) 
    # TODO: Rimettere i lock  
    self.clients -= 1

  '''
    Metodi eseguiti da __connected
    Vengono richiamati dinamicamente attraverso self.dynamic[key]
  '''

  def dyn_ADM_DEBUG        ( self, conn, data ):
    '''
      Inverte il flag self.printdebug per visualizzare o nascondere i messaggi sul server
    '''
    # data['msg'] == MsgPrt.ADM_DEBUG:
    p = self.get_objpayload( data )             # Credenziali inviate dal client
    if self.isAdmin( p['key'], p['secret'] ):
      if p['flag'] == 'f_debug':
        self.printdebug ^= True
      if p['flag'] == 'f_alert':
        self.printalert ^= True
      if p['flag'] == 'f_errors':
        self.printerr   ^= True
      try:
        obj = self.set_objcnn( MsgPrt.ADM_DEBUG_OK, {'f_debug': self.printdebug, 'f_alert': self.printalert, 'f_errors': self.printerr} )
        self.sendall( conn, obj )

      except Exception as e:
        self.printErr( Err=e, Mess="" )
    else:
      try:
        obj = self.set_objcnn( MsgPrt.NOT_ADMIN )
        self.sendall( conn, obj )

      except Exception as e:
        self.printErr( Err=e, Mess="" )

  def dyn_ADM_CLIENT_DB    ( self, conn, data ):
    # data['msg'] == MsgPrt.ADM_CLIENT_DB:
    p = self.get_objpayload( data )             # Credenziali inviate dal client
    if self.isAdmin( p['key'], p['secret'] ):
      try:
        obj = self.set_objcnn( MsgPrt.ADM_CLIENT_DB_OK, self.data['clientdb'] )
        self.sendall( conn, obj )

      except Exception as e:
        self.printErr( Err=e, Mess="" )
    else:
      try:
        obj = self.set_objcnn( MsgPrt.NOT_ADMIN )
        self.sendall( conn, obj )

      except Exception as e:
        self.printErr( Err=e, Mess="" )

  def dyn_ADM_CLIENT_ONLINE( self, conn, data ):
    # data['msg'] == MsgPrt.ADM_CLIENT_ONLINE:
    p = self.get_objpayload( data )             # Credenziali inviate dal client
    if self.isAdmin( p['key'], p['secret'] ):
      try:
        obj = self.set_objcnn( MsgPrt.ADM_CLIENT_ONLINE_OK, self.onlinedata['tokens'] )
        self.sendall( conn, obj )

      except Exception as e:
        self.printErr( Err=e, Mess="" )
    else:
      try:
        obj = self.set_objcnn( MsgPrt.NOT_ADMIN )
        self.sendall( conn, obj )

      except Exception as e:
        self.printErr( Err=e, Mess="" )

  def dyn_SIGNUP           ( self, conn, data ):
    # data['msg'] == MsgPrt.SIGNUP:
    cliobj  = self.creaClient()
    self.debug( cliobj )
    try:
      obj  = self.set_objcnn( MsgPrt.SIGNUP_OK, cliobj )
      data = self.sendrecv( conn, obj )

      m    = self.get_objmsg( data )
      if m == MsgPrt.ACK:
        self.addClient( cliobj )
        
    except Exception as e:
      self.printErr( Err=e, Mess="Errore {0}".format( cliobj['clientname'] ) )

  def dyn_LOGIN            ( self, conn, data ):
    # data['msg'] == MsgPrt.LOGIN:
    p = self.get_objpayload( data )             # Credenziali inviate dal client
    
    client = self.getClient( p['key'], p['secret'] )
    if client != None:              # Credenziali trovate
    
      # Invio token
      key             = p['key']
      secret          = p['secret']
      dbtoken = self.getToken( key, secret )
      if dbtoken != None:         # Trovato token ancora valido per il client
        login = dbtoken['created']
        token = dbtoken['token']
      else:                       # Generazione nuovo token
        (login, token)  = self.newToken()

      # Invio token
      obj  = self.set_objcnn( MsgPrt.LOGIN_OK, {'token': token} )
      try:                        # Conferma ricezione token
        data = self.sendrecv( conn, obj )

        m    = self.get_objmsg( data )
        if m == MsgPrt.ACK:
          if dbtoken == None:     # Se il token e' stato generato lo salvo nel db
            self.addToken( login, token, key, secret )

      except Exception as e:
        self.printErr( Err=e, Mess="Errore {0}".format( client['clientname'] ) )
          
    else:                           # Credenziali non trovate
      obj = self.set_objcnn( MsgPrt.CREDNOTVALD )
      self.sendall( conn, obj )
 
  def dyn_JOBREQUEST( self, conn, data ):
    # Il client invia JOBREQUEST per chiedere nuovi task da elaborare
    # data['msg'] == MsgPrt.JOBREQUEST:
    p = self.get_objpayload( data )             # Credenziali inviate dal client
    
    client = self.getClient( p['key'], p['secret'] )
    if client != None:              # Credenziali trovate
    
      if self.isOnline( p['key'], p['secret'], p['token'] ):
       
        sp = {'data': {}}
        if len( self.jobs ) > 0:
          # Evito concorrenza assegnazioni con altri thread
          while self.assigning_job:
            pass
          # Inizio assegnazione
          self.assigning_job = True 
          for jid, j in self.jobs.items():
            if j['status'] in [JobStatus.NEW, JobStatus.RESEND]:
              # Assegno il job da elaborare
              self.jobsetstatus( jid, JobStatus.WORKING )
              j['token']   = p['token']
              j['sent']    = time.time()
              j['received']= 0
              # aggiungo i job all' oggetto da inviare
              sp['data'][ jid ] = j
              self.data['jobs_io'][ jid ]['changed'] = True
              break

          # Fine assegnazione
          self.assigning_job = False 

        # Invio richiesta di elaborazione o segnale nulla da fare
        if len( sp['data'] ) > 0:
          obj = self.set_objcnn( MsgPrt.JOBREQUEST_CALC, sp )
        else:
          obj = self.set_objcnn( MsgPrt.JOBREQUEST_NOTHING )
 
        try:
          rec = self.sendrecv( conn, obj )
          m   = self.get_objmsg( rec )

          if m == MsgPrt.ACK:
            # Tutto ok
            pass
          if m == MsgPrt.ERROR:
            # Rollback task assegnati al client tramite token
            for jid, j in sp['data'].items():
              self.alert( "Rollback JOB {0}".format( jid ) )
              self.jobsetstatus( jid, JobStatus.NEW )
              j['token']  = ''
              j['sent']   = 0

        except Exception as e:      # Mancata risposta
          self.printErr( Err=e, Mess="Client: {0}".format( client['clientname'] ) )
    
      else:
        obj = self.set_objcnn( MsgPrt.NOT_LOGGED )
        self.sendall( conn, obj )

    else:                           # Credenziali non trovate
      obj = self.set_objcnn( MsgPrt.CREDNOTVALD )
      self.sendall( conn, obj )
  
  def dyn_JOBDONE( self, conn, data ):
    # data['msg'] == MsgPrt.JOBDONE:
    p = self.get_objpayload( data )             # Credenziali inviate dal client
    
    client = self.getClient( p['key'], p['secret'] )
    if client != None:              # Credenziali trovate
    
      if self.isOnline( p['key'], p['secret'], p['token'] ):
        obj = self.set_objcnn( MsgPrt.ACK )
        self.sendall( conn, obj )
        
        # p['data'] contiene i job elaborati
        rec_jobs = p['data'] 
        for idrj, rj in rec_jobs.items():
          tot_time = 0
          b_junk = False
          # cerco i job ricevuti nei job sul server
          if rj['id'] in self.jobs:
            # seleziono job su server
            j = self.jobs[ rj['id'] ]
            # inizio controlli
            b_junk = False
            # controllo id
            b_junk = b_junk or ( rj['id']     != j['id'] )
            # controllo che il job ricevuto sia stato elaborato con lo stesso algoritmo
            b_junk = b_junk or ( rj['func']   != j['func'] )
            # controllo time invio
            b_junk = b_junk or ( rj['sent']   != j['sent'] )
            # controllo token invio
            b_junk = b_junk or ( rj['token']  != j['token'] )
            #controllo stato
            b_junk = b_junk or ( rj['status'] != JobStatus.DONE )
            # eventuali altri controlli sul job ricevuto qui...
            if b_junk == False:
              # copio task ricevuti
              for rtid, rt in rj['tasks'].items():
                # cerco i task ricevuti nel job sul server
                if rt['id'] in j['tasks']:
                  # seleziono task nel job del server
                  t = j['tasks'][ rt['id'] ]
                  # inizio controlli
                  b_junk = False
                  # controllo sui dati
                  b_junk = b_junk or ( rt['data'] != t['data'] )
                  #controllo stato
                  b_junk = b_junk or ( rt['status'] != TaskStatus.DONE )
                  if b_junk == False:
                    # il task ricevuto sembra valido
                    if j['task_done'] < j['task_num']:
                      j['task_done'] += 1
                    if j['task_done'] == j['task_num']:
                      self.jobsetstatus( j['id'], JobStatus.DONE )
                    t['start']    = rt['start']
                    t['end']      = rt['end']
                    t['result']   = rt['result']
                    t['status']   = TaskStatus.DONE

                    tot_time += ( rt['end'] - rt['start'] )

                    # eventuali statistiche sul client che ha prodotto i risultati...
                    self.exec_call_back( self.cb_jobdone_stats, rj, rt )
                  else:
                    self.alert( "Task {0} scartato".format( rt['id']) )
                else:
                  self.alert( "Task {0} scartato".format( rt['id']) )
              # Assegno attributi job
              # t['sent'] valorizzato durante l'invio
              j['received'] = time.time()
              j['el_time']  = tot_time
              self.data['jobs_io'][ j['id'] ]['changed'] = True

            else:
              self.alert( "Job {0} scartato".format( rj['id']) )
          else:
            self.alert( "Job {0} scartato".format( rj['id']) )

          if b_junk:
            self.alert( "Ho trovato degli errori controllando i dati ricevuti:" )
            self.alert( "Job server:" )
            self.showjobs( { j['id']:   j } )
            self.alert( "Job ricevuto:" )
            self.showjobs( { rj['id']: rj } )

      else:
        obj = self.set_objcnn( MsgPrt.NOT_LOGGED )
        self.sendall( conn, obj )

    else:                           # Credenziali non trovate
      obj = self.set_objcnn( MsgPrt.CREDNOTVALD )
      self.sendall( conn, obj ) 

  '''
    Gestione dati client
  '''

  def creaClient( self ):
    clinum  = len( self.data['clientdb'] )
    cliname = "CLIENT-{0:0>4n}".format( clinum )
    created = time.time()
    skey    = hashlib.sha224( cliname ).hexdigest()
    secret  = hashlib.sha224( "{0:14.3f}".format( created ) ).hexdigest()
    if clinum == 0:
      secur = 'ADMIN'
    else:
      secur = 'USER'
    cliobj = {'clientname': cliname, 'created': created, 'key': skey, 'secret': secret, 'secur': secur}
    
    return cliobj

  def addClient( self, cliobj ):
    self.data['clientdb'].append( cliobj )
    self.data['io']['changed'] = True

  def getClient( self, key, secret ):
    res = None
    for c in self.data['clientdb']: # Cerco le credenziali ricevute nel db
      if c['key'] == key and c['secret'] == secret:
        res = c
        break
    return res

  def isAdmin( self, key, secret ):
    client = self.getClient( key, secret )
    return client['secur'] == 'ADMIN'

  #  Gestione dati Token

  def newToken( self ):
    # Crea un token senza memorizzarlo
    created = time.time()
    token   = hashlib.sha224( "{0:14.3f}".format( created ) ).hexdigest()
    return (created, token)

  def addToken( self, created, token, key, secret ):
    # Memorizza i dati del token
    obj = { 'token': token, 'key': key, 'secret': secret, 'created': created, 'expire': created+(12*60*60) }
    self.onlinedata['tokens'].append( obj )

  def getToken( self, key, secret ):
    res = None
    for t in self.onlinedata['tokens']:  # Cerco i token rilasciati
      if t['key'] == key and t['secret'] == secret:
        res = t
        break
    return res

  def validateToken( self, key, secret, token ):
    res = self.getToken( key, secret )
    if res != None:
      if res['token'] != token:
        return None
    return res

  def isOnline( self, key, secret, token ):
    token = self.validateToken( key, secret, token )
    #self.debug( "isOnline {0}".format( token ) )
    return token != None
    

