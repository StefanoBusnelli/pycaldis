#!/usr/bin/python
import dill
import zlib
import sys


def show( obj=None, lev=0 ):

  def spazi( lev ):
    if lev >= 2:
      return " "*(20*(lev-2))
    else:
      return ""

  def ramo( lev ):
    if lev > 1:
      return "\-----------------  "
    else:
      return ""

  s = ""
  lev += 1
  if type( obj ) == dict:
    if obj == {}:
      s = "{}\n"
    else:
      n = 0
      for k, v in obj.items():
        if n == 0:
          s = s + "{0:<18}: {1}".format( k, show( v, lev ) )
        else:
          s = s + "{0}{1}{2:<18}: {3}".format( spazi(lev), ramo(lev), k, show( v, lev ) )
        n += 1

  elif type( obj ) == list:
    if obj == []:
      s = "[]\n"
    else:
      n = 0
      for v in obj:
        if n == 0:
          s = s + "{0:<18}: {1}".format( n, show( v, lev ) )
        else:
          s = s + "{0}{1}{2:<18}: {3}".format( spazi(lev), ramo(lev), n, show( v, lev ) )
        n += 1

  elif type( obj ) == str:
    s = s + "{0:<20}\n".format( str( obj ) )

  else:
    s = s + "{0:>20}\n".format( str( obj ) )

  return s
    
reload( sys )
sys.setdefaultencoding('utf8')

if len(sys.argv) == 2:
  fname = str( sys.argv[1] )
  file  = open( fname, 'r' )
  zdata = file.read()
  sdata = zlib.decompress( zdata ) 
  data  = dill.loads( sdata )
  print show( data )
