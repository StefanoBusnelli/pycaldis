#!/usr/bin/python
import dill
import zlib
import sys

data = {0: 0, 1: 1, 2: 2}
sdata= dill.dumps( data )
zdata= zlib.compress( sdata )

file = open( "test.data", "w" )
file.write( zdata )
file.flush()
file.close()

