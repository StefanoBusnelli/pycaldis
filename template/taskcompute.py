def taskcompute( task={} ):
  # Elaborazione task, funzione di esempio che deve essere reimplementata nella classe derivata
  if len( task ) > 0:
    print( "{0:<8s} {1:>8d} {2:<8s} {3:>2s}".format( "Task:", task['id'], "State:", task['status'] ) )
    print( "\t{0:<8s} {1:>20s} {2:<8s} {3:>20s}".format( "Data:", task['data'], "Result:", task['result'] ) )
    print( "\t{0:<8s} {1:>20s}".format( "Token:", task['token'] ) )

    print( "\tElaboro..." )

    return [-1, None]

  else:
    print( "Vuoto" )
    
    return None

