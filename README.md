![alt text](./img/pycaldis.png "PyCalDis")

## Ver 1.4 ##

## Install ##

```
pip install setuptools

pip install .
```

## Features ##

* No connection needed on computing. Clients start comunication with the server just when needed for asking credentials, asking jobs or sending results.
* Clients and Servers saves internal status and configuration on file, so they can be stopped and restarted ( even after crash ) without data loss.
* Scalable. Client and Server Class can be extended in derived python class for grid application.

```
from pycaldis import Client, Server
class FooClient( Client ):
  def __init__( self, host='127.0.0.1', port=4096 ):
    super( MBClient, self ).__init__( host, port )
  .
  .
  .
  
class FooServer( Server ):
  def __init__( self, port=4096 ):
    super( FooServer, self ).__init__( port )
  .
  .
  .
  
```

## Description ##

Pycaldis implements two class that can be extended for creating grid computing application.
Clients starts communication with the server when they wants someting to do, on computing they are not connected.

### Data structures ###

IO data structure:

```
{
  fname:       string
  changed:     bool
  reading:     bool
  writing:     bool
}
```

JobIO data structure:

```
{
  id:          number
  fname:       string
  thread:      thread object ref
  changed:     bool
  load:        bool
  error:       bool
  writing:     bool
  reading:     bool
  unloading:   bool
}
```

Job structure:

```
{
  id:          number
  data:        []
  desc:        string
  task_num:    number
  task_done:   number
  status:      string
  func         string
  token        string
  sent         time
  received     time
  el_time      time
  tasks:       {}
}
```

Task structure:

```
{ 
   id:          number 
   data:        []
   result:      []
   status:      string
   start:       number
   end:         number
}
```

### Server ###

Impplements a network service that listen for client's request and distribute them some tasks to be executed.

This class starts a server that listen on port a given port (default 4096 ) that will create jobs made by task_per_job tasks. More the task per job, bigger the data size exchanged.

```
from pycaldis import Server
S=Server( port=4096, task_per_job=2000, max_data_file=16 )
S.Listen()
```

Tasks are grouped in jobs and each job has a python function 'taskcompute' that is sent to clients.
Server creates the jobs and relatives tasks, and provide the py file that defines the taskcompute function.

There is not limitation upon what taskcompute() can do, but:
  *  The function name must be taskcompute
  *  It must accept an object with the task strucure and do domething with the task['data'] list, then return a list with the results that will be saved in task['result'].
taskcompute() must be declared in a file:

job0.py

```
def taskcompute( ( task={} ):
  res = []
  # do something with task['data'] and insert values in ret
  return res
```
     
When the server creates the job, it inserts the content of the file in job['func'] so the filename must be provided:

```
from pycaldis import Server
S=Server()
S.jobcreate( [], 'jobdesc', 'job0.py' )
```

  * Parameters:
    *  port
       Set the TCP port of the server
    *  max_tasks_per_job
       Sets the number of tasks to be sent in one job for being processed in dyn_JOBREQUEST
    *  max_data_file
       Sets the number of job's datafile in state NEW to be kept in memory (self.jobs) for being sent to workers. This saves the memory needing. Datafile will be automatically unload when in status DONE and loaded when the number of datafiles is less than max_data_file. This number must be raised as the number of workers grows.

  * Base methods:
    *  Listen()
       The server starts listening for requests
    *  jobcraete()
       Create a job object
    *  jobadd()
       Add the job object to the dictionary data['jobs']
    *  taskcreate()
       Create a task object
    *  taskadd()
       Add a task object to a job object dictionary j['tasks']
    * start_autosave
      Starts a thread that saves data when the flag changed is set or every a given timeout in secods.

  * call back functions:
    *  cb_jobdone_stats
       called on every task received and correctly processed.
       takes 2 params: ( job_object, task_object )
       Usefull for calculating stats with task_object['result']

```
def stats_cb( self, j, t ):
  self.stats.append( t['result'] )
  #...
self.stats = []
self.cb_jobdone_stats = self.stats_cb
```

### Client ###
  
Implements a client that talks with the server, ask for jobs and send back results.
 
This class starts a client that talk with a server listening on ip 192.168.1.10 and port 4096

```
from pycaldis import Client
C=Client( host="192.168.1.10", port=4096 )
C.Signup()
C.Login()
C.JobRequest()
C.Compute()
C.JobDone()
```
 
  * Parameters:
    * host
      Set the server's address to connect
    * port
      Set the TCP port of the server to connnect
    * max_tasks_per_job
      Sets the number of tasks to be sent in one job for being processed in dyn_JOBREQUEST
 
  * Base methods:
    * Signup()
      Ask credentials
    * Login()
      Ask a login token
    * JobRequest()
      Ask job to process
    * Compute()
      Compute jobs with the serialized function received
    * JobDone()
      Send results of the job processed
    * start_autosave
      Starts a thread that saves data when the flag changed is set or every a given timeout in secods.

## Example: ##

### Server ###

taskcompute.py:

```
def taskcompute( task={} ):
  print "taskcompute from FooServer:"
  if len( task ) > 0:
    print( "{0:<8s} {1:>8d} {2:<8s} {3:>2s}".format( "Task:", task['id'], "State:", task['status'] ) )
    print( "\t{0:<8s} {1:>20s} {2:<8s} {3:>20s}".format( "Data:", task['data'], "Result:", task['result'] ) )
    print( "\t{0:<8s} {1:>20s}".format( "Token:", task['token'] ) )

    print( "\tElaboro..." )

    return [-1, None]

  else:
    print( "Vuoto" )
    
    return None
```

FooServer.py:

```
from pycaldis import Server

class FooServer( Server ):

  def __init__( self, port=4096, task_per_job=1024, max_data_file=16 ):
    super( FooServer, self ).__init__( port )

    self.data['jobs'] = {} 
    j=self.jobcreate( [], '0.0, 0.0', 'taskcompute.py' )

    t=self.taskcreate( [0.0, 0.0] )
    self.taskadd( j, t )
    t=self.taskcreate( [0.0, 0.1] )
    self.taskadd( j, t )
    t=self.taskcreate( [0.0, 0.2] )
    self.taskadd( j, t )
    t=self.taskcreate( [0.0, 0.3] )
    self.taskadd( j, t )
    
    self.jobadd( j )
    
    self.Listen()

S=FooServer( port=12345, task_per_job=2048, max_data_file=32 )

```

### Client ###

FooClient.py:

```
from pycaldis import Client

class FooClient( Client ):

  def __init__( self, host='127.0.0.1', port=4096 ):
    super( MBClient, self ).__init__( host, port )

    self.data['jobs'] = {}

    if 'clientname' not in self.data:
      try:
        self.Signup()
      except:
        self.alert( "Errore signup" )

    if 'clientname' in self.data:
      try:
        self.Login()
      except:
        self.alert( "Errore di login" )

    try:
      self.Run()
    except:
      self.alert( "errore Run" )

  def Run( self ):
 
    b_finish = False
    while ( b_finish == False ):
      self.alert( "Chiedo job" )
      try:
        self.JobRequest()

        b_finish = ( len( self.data['jobs'] ) == 0 )
        if b_finish == False:
          self.alert( "  Elaboro" )
          self.Compute()
          for idj, j in self.data['jobs'].items():
            if j['status'] == JobStatus.DONE:
              self.alert( "  Invio risultati" )
              self.JobDone()
            else:
              self.alert( "Non invio niente." )
        else:
          self.alert( "Fine" )

      except E_CredNonVald:
        seld.alert( "Rieseguo signup" )
        self.Signup()
      except E_NoLogin:
        self.alert( "Rieseguo login" )
        self.Login()

C=FooClient( host="192.168.1.10", port=12345 )

```

## Running projects ##

* [Mandelbrot](https://bitbucket.org/StefanoBusnelli/mandelbrot/overview)
  ``` 
  git clone git@bitbucket.org:StefanoBusnelli/mandelbrot.git
  ```

