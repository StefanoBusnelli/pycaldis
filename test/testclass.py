from pycaldis import Server

class TS(Server):
  def __init__( self, port=4096, task_per_job=10, max_data_file=16 ):
    super( TS, self ).__init__( port=port, task_per_job=task_per_job, max_data_file=max_data_file )

    if len( self.jobs ) == 0:
      print "Creo job"
      j={}
      for jid in range( 2 ):
        j[jid] = self.jobcreate( [jid], str(jid), '' )
        t={}
        for tid in range( task_per_job ):
          t[tid] = self.taskcreate( [tid] )
          self.taskadd( j[jid], t[tid] )
        self.jobadd( j[jid] )

    self.start_internalcheck()

    print "Pronto"

  def show( self ):
    print self.data['io']
    for k, v in self.data['jobs_io'].items():
      print "{0:8d}{1}".format( k, v )
