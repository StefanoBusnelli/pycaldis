#!/usr/bin/python
import time
from pycaldis import Client, E_NothingToDo, E_ExecError

c=Client()

try:

  print c.data

  if 'clientname' not in c.data:
    c.Signup()

  c.Login()

#  try:
#    print "\nServerToggleDebug"
#    c.ServerToggleDebug()
#  except pycaldis.E_NotAdmin as e:
#    print "Non sei admin"
#    pass

#  try:
#    print "\nServerToggleAlert"
#    c.ServerToggleAlert()
#  except pycaldis.E_NotAdmin as e:
#    print "Non sei admin"
#    pass

#  try:
#    print "\nServerToggleError"
#    c.ServerToggleError()
#  except pycaldis.E_NotAdmin as e:
#    print "Non sei admin"
#    pass

#  try:
#    print "\nClientDb"
#    c.ClientDb()
#  except pycaldis.E_NotAdmin as e:
#    print "Non sei admin"
#    pass
#
#  try:
#    print "\nClientOnline"
#    c.ClientOnline()
#  except pycaldis.E_NotAdmin as e:
#    print "Non sei admin"
#    pass

  print c.data

  if 'jobs_io' not in c.data or len( c.jobs ) == 0:
    b_Redo = True
    while b_Redo:
      try:
        print "\nJobRequest"
        c.JobRequest()
        b_Redo = False
      except E_NothingToDo as e:
        print "Non c'e' niente da fare"
        b_Redo = False
        pass
      except E_ExecError as e:
        print "errore esecuzione"
        time.sleep( 5 )
        pass
      except Exception as e:
        print e
        b_Redo = False
        pass

  print c.data

  if 'jobs_io' in c.data and len( c.jobs ) == 1 and c.jobs[0]['status'] == 0:
    print "\nCompute"
    c.showtasks()
    c.Compute()
    c.showtasks()

  print c.data

  if 'jobs_io' in c.data and len( c.jobs ) == 1 and c.jobs[0]['status'] == 2:
    print "\nJobDone"
    c.JobDone()

  print c.data


except Exception as e:
  print e
