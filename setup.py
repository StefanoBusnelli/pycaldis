from setuptools import setup

setup(	name		= 'pycaldis',
	version		= 1.4,
	description	= 'Applicazione client-server di calcolo distribuito',
	url		= 'https://bitbucket.org/StefanoBusnelli/pycaldis',
	author		= 'Busnelli Stefano',
	author_email	= 'busnelli.stefano@gmail.com',
	license		= 'GNU GPLv3',
	packeges	= ['pycaldis'],
        install_requires= ['Enum','dill'],
	zip_safe	= False
)
